## 1.27.2-27
- Hỗ trợ đèn CCT mới

## 1.27.2-26
- Hỗ trợ đèn RGB mới

## 1.27.2-25
- Hỗ trợ khóa Yale ffff200a00
- Hỗ trợ thêm driver zigbee 12W mới _TZ3210_0tryeeg2

## 1.27.2-24
- Hỗ trợ khóa Yale ffff200a00

## 1.27.2-23
- Hỗ trợ bộ aptomat 1 pha mới

## 1.27.2-22
- Hỗ trợ đèn downlight 10W

## 1.27.2-21
- Hỗ trợ đèn led ray thông minh

## 1.27.2-20
- Hỗ trợ module dimmer 2 kênh mới

## 1.27.2-19
- Hỗ trợ đèn downlight Zigbee Javis 12W mới
- Hỗ trợ remote điều khiển đèn gắn tường Zigbee mới
- Hỗ trợ cảm biến chuyển động siêu âm 24G MSA053

## 1.27.2-18
- Hỗ trợ đèn downlight Zigbee mới

## 1.27.2-17
- Hỗ trợ module Aqara T2
- Hỗ trợ cảm biến hiện diện 24G MSA201

## 1.27.2-16
- Hỗ trợ đèn Aqara mới

## 1.27.2-15
- Hỗ trợ bộ điều khiển Daikin VRV mới

## 1.27.2-14
- Hỗ trợ công tắc cửa cuốn Zigbee mới

## 1.27.2-13
- Hỗ trợ thiết lập trạng thái công tắc sau khi mất điện cho công tắc 2,3 nút

## 1.27.2-12
- Hỗ trợ bộ đo điện Javis mới
- Hỗ trợ đèn sưởi Aqara T1

## 1.27.2-11
- Hỗ trợ bộ đo điện Javis mới

## 1.27.2-10
- Hỗ trợ đèn Aqara T2, T3
- Hỗ trợ cảm biến cửa HM loại mới
- Cải tiến kết nối của cảm biến chuyển động PIR âm trần 220v 

## 1.27.2-9
- Hỗ trợ module điều khiển rèm
- Cập nhật cảm biến chuyển động PIR âm trần 220v 

## 1.27.2-8
- Hỗ trợ bộ điều khiển điều hòa Daikin Zigbee
- Hỗ trợ tính năng thiết lập trạng thái công tắc sau khi mất điện (ON/OFF/trạng thái cũ) cho một số mẫu công tắc mới

## 1.27.2-7
- Sửa lỗi khoá cửa JS-SLK1
- Hỗ trợ chỉnh công suất phát của router 2652

## 1.27.2-6
- Hỗ trợ module dimmer 220v mới
- Hỗ trợ cảm biến chuyển động 220V mới
- Hỗ trợ moter rèm mới
- Sửa lỗi công tắc 4 nút

## 1.27.2-4
- Hỗ trợ set trạng thái sau khi mất điện cho 1 số mẫu công tắc

## 1.27.2-3
- Hỗ trợ cảm biến khói mới
- Hỗ trợ cảm biến nhiệt độ có màn hình mới
- Hỗ trợ cảm biến chuyển động Aqara E1

## 1.27.2-2
- Hỗ trợ cảm biến khói Heiman
- Hỗ trợ bộ định vị ô tô thông minh JAVIS

## 1.27.2-1
- Cập nhật tên 1 số thiết bị

## 1.27.2
- Cập nhật bản zigbee dựa trên phiên bản Zigbee2mqtt 1.27.2