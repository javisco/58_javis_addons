## 1.1.1
- Fix lỗi khởi động lại HC một số trường hợp không tự start dịch vụ

## 1.1.0
- Bổ sung tính năng kiểm tra trạng thái kết nối mqtt và tự restart lại dịch vụ

## 1.0.1
- Cải tiến phần quản lý log tăng hiệu năng

## 1.0.0
- Phiên bản đầu tiên cho phép tự cấu hình thông số điều hoà Daikin VRV