# Get HACS

Add-on để cài đặt HACS .

> **This add-on does not run HACS. This add-on only provides the easiest way to download HACS.**

## Instructions

1. Bấm **Install** để cài đặt add-on.
2. Bấm start để chạy add-on.
3. Chuyển sang thẻ log kiểm tra cho dến khi báo successful
4. Khởi động lại HA

> [!NOTE]
> When you see the line "Remember to restart Home Assistant before you configure it" in the logs,
> the downloader has completed, and you need to do what it tells you (restart Home Assistant).
> At this point, you can uninstall the add-on as you would no longer need it.
> The rest of the operation is done in the Home Assistant UI.
> Look at the [documentation](https://hacs.xyz/docs/use/configuration/basic/) for more information.
