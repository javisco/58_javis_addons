# Javis Addons: JAVIS HC TOOL
Công cụ quản lý bộ trung tâm JAVIS HC. Phiên bản DEV có thể còn phát sinh lỗi.
## Hỗ trợ

Nếu bạn cần hỗ trợ xin vui lòng kết nối với JAVIS ở các kênh dưới đây:
https://www.facebook.com/nhathongminh.io

## Cập nhật sản phẩm mới
- https://nhathongminh.io
- https://javishome.vn 

[aarch64-shield]: https://img.shields.io/badge/aarch64-yes-green.svg
[amd64-shield]: https://img.shields.io/badge/amd64-yes-green.svg
