## 1.6.0:
- Hỗ trợ tự động cập nhật thời gian 5p/lần
- Cải tiến tính năng xóa log tự động để tối ưu dung lượng

## 1.5.5:
- Hỗ trợ update OTA cho gateway báo khói liên động không dây

## 1.5.3:
- Update monitor

## 1.5.2:
- Update reset zigbee

## 1.5.1:
- Fix monitor

## 1.4.3:
- Cập nhật cấu hình điều hòa Daikin VRV

## 1.4.2:
- Cập nhật giao diện
- Cập nhật cấu hình Maika
- Cập nhật link quản lý automation và kịch bản

## 1.4.1:
- Cập nhật tính năng xóa các file backup zigbee, không xóa tất
- Hỗ trợ tính năng cập nhật SmartIR bằng HC tool

## 1.4.0:
- Hỗ trợ tính năng reset zigbee về mặc định như xuất xưởng
- Cập nhật smart IR bản 1.16.4
- Cập nhật code hồng ngoại mới

## 1.3.9:
- Hỗ trợ stop zigbee (không cho tự động chạy)
- Bỏ phần quản lý fan hồng ngoại (không tương thích)

## 1.3.8:
- Sửa lỗi cấu hình bộ điều khiển Daikin VRV

## 1.3.7:
- Sửa lỗi kết nối điều hoà Daikin VRV

## 1.3.6:
- Cập nhật tính năng kết nối loa Maika
- Fix lỗi telegram

## 1.3.5:
- Cập nhật tính năng cập nhật dịch vụ hệ thống

## 1.3.4:
- Bổ sung điều hoà WHIRLPOOL_AC
- Cập nhật tính năng hệ thống

## 1.3.3:
- Cải tiến chức năng tạo bộ phản hồi điều hoà
- Bổ sung tính năng xoá file backup zigbee2mqtt

## 1.3.2:
- Cải tiến học lệnh IR, RF cho bộ điều khiển điều hoà, TV, Quạt

## 1.3.1:
- Fix lỗi đổi password HC tool

## 1.3.0:
- Cải tiến chức năng học lệnh dùng Broadlink
- Cải tiến chức năng quản lý camera

## 1.2.8:
- Fix lỗi cấu hình rèm mqtt
- Thêm tool gen person_javis.json
- Fix cấu hình bộ alarm

## 1.2.6:
- Fix lỗi không nhận bộ RM4c pro khi để tên tiếng Việt

## 1.2.5:
- Fix lỗi học lệnh Broadlink

## 1.2.4:
- Fix lỗi quét QR code cho app Javis home

## 1.2.3:
- Bổ sung tính năng gán nhãn khuôn mặt bằng camera AI

## 1.2.2:
- Bổ sung tính năng kiểm tra kết nối HC nội bộ và cloud
- Bổ sung tính năng dọn dẹp log
- Fix lỗi tính năng cấu hình công tắc MQTT

## 1.1.8:
- Cải tiến tự động nhà vệ sinh không tự động chạy khi restart dịch vụ HC
- Cải tiến phần quản lý spotify
- Khắc phục lỗi học lệnh hồng ngoại và RF trên một số model Broadlink
- Sửa lại code học lệnh bộ điều khiển điều hoà
- Cải tiến phần thiết lập Telegram
- Bổ sung tính năng kiểm tra các dịch vụ HC, kiểm tra kết nối đến cloud

## 1.1.7:
- Cải tiến chức năng học lệnh RF
- Hỗ trợ tính năng cập nhật smart ir
- Bổ sung các dịch vụ phân tích HC và xoá log giải phóng dung lượng

## 1.1.6:
- Cải tiến chức năng học lệnh Broadlink
- Cải tiến chức năng cấu hình telegram, hỗ trợ group thông báo

## 1.1.5:
- Fix lỗi chức năng học lệnh Broadlink
- Fix lỗi cấu hình Spotify, Telegram

## 1.0.9:
- Cải tiến chức năng học lệnh Broadlink
- Hỗ trợ cấu hình Spotify
- Hỗ trợ cấu hình Telegram
- Fix lỗi tự động hoá kết hợp khoá cửa

## 1.0.8
- Bỏ phần cập nhật phiên bản mới trong menu Hệ thống
- Fix lỗi login khi HC có ảnh trong thư mục /tmp/camera
- Fix tạo công tắc mqtt từ công tắc wifi

## 1.0.1
- Phiên bản đầu tiên