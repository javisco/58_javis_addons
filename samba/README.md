# Javis Addons: SAMBA share
Addon chia sẻ dữ liệu từ bộ trung tâm Javis HC. Mục đích chính dùng để sao lưu dữ liệu và upload nhạc vào bộ trung tâm Javis HC.
## Hỗ trợ

Nếu bạn cần hỗ trợ xin vui lòng kết nối với JAVIS ở các kênh dưới đây:
https://www.facebook.com/nhathongminh.io

## Cập nhật sản phẩm mới
- https://nhathongminh.io
- https://javishome.vn 

[aarch64-shield]: https://img.shields.io/badge/aarch64-yes-green.svg
[amd64-shield]: https://img.shields.io/badge/amd64-yes-green.svg
