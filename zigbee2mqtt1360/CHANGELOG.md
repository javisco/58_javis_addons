## LƯU Ý
- Không được nâng cấp từ bản zigbee cũ (ví dụ từ bản zigbee 1272 lên bản zigbee 1360 này). 
- Nếu nâng cấp từ bản cũ 1182, 1183 hoặc 1272 lên bản mới này sẽ gây lỗi toàn bộ các thiết bị.
- Chỉ tương thích với bản HA 2024.4.4 trở lên

## 1.41.0-8
- Cải tiến điều hòa VRV (2 mã)
- Cải tiến bộ định vị ô tô
- Cải tiến máy sưởi Aqara T1

## 1.41.0-7
- Fix công tắc kịch bản 1 gang

## 1.41.0-6
- Fix cập nhật trạng thái máy sưởi Aqara T1

## 1.41.0-5
- Hỗ trợ tính năng AI cho cảm biến MSA201ZN

## 1.41.0-3
- Hỗ trợ công tắc JAVIS luxury 1,2,3,4 gang version 2025.

## 1.41.0-2
- Hỗ trợ cảm biến hiện diện MSA201Z mới

## 1.41.0-2
- Hỗ trợ cảm biến hiện diện MSA201Z mới

## 1.41.0-1
- Hỗ trợ công tắc kịch bản cơ 4 gang
- Fix lõi máy sưởi Aqara T1

## 1.40.2-1
- Hỗ trợ cảm biến FP1-E
- Hỗ trợ công tắc JAVIS luxury version 2025.

## 1.40.1-1
- Hỗ trợ loa thông báo tiếng Việt

## 1.40.0-1
- Hỗ trợ công tắc kịch bản

## 1.39.1-4
- Hỗ trợ cảm biến chuyển động zigbee vuông có chỉnh phạm vi

## 1.39.1-3
- Cải tiến điều hòa trung tâm Siemens (Chiller)

## 1.39.1-2
- Hỗ trợ điều hòa trung tâm Siemens

## 1.39.1-1
- Hỗ trợ bản z2m 1.39.1

## 1.39.0-3
- Hỗ trợ bộ điều khiển điều hòa Siemens chiller làm mát bằng nước

## 1.39.0-2
- Update z2m 1.39.0

## 1.38.0-5
- Fix cảm biến 220V PIR

## 1.38.0-3
- Fix cảm biến khói

## 1.38.0-2
- Fix kết nối máy sưởi Aqara
- Fix công tắc TS0002

## 1.38.0-1
- Dựa trên bản z2m 1.38.0-1
- Hỗ trợ kết nối zigbee HC phụ qua LAN

## 1.37.1-2
- Hỗ trợ công tắc JAVIS đèn nền RGB
- Hỗ trợ ổ cắm âm tường có sạc nhanh

## 1.37.1-1
- Cải tiến hiển thị thông tin của cảm biến hiện diện 053, 201z
- Hỗ trợ USB zigbee mới

## 1.36.1-5
- Thêm thuộc tính của các cảm biến hiện diện
- Đổi đúng tên 1 số thiết bị

## 1.36.1-4
- Cải tiến cảm biến hiện diện 201 và 503

## 1.36.1-3
- Hỗ trợ ổ cắm thông minh JAVIS mới
- Hỗ trợ công tắc ổ cắm JAVIS

## 1.36.1-2
- Tổ chức lại cấu trúc các thiết bị JAVIS

## 1.36.1-1
- Hỗ trợ công tắc đèn nền RGB và thay đổi độ sáng

## 1.36.1
- Cập nhật bảng z2m 1.36.1

## 1.36.0
- Cập nhật bảng z2m 1.36.0

## 1.31.2-5
- Hỗ trợ bộ đo điện 3 pha và cảm biến hiện diện 220v

## 1.31.2-4
- Hỗ trợ đèn downlight Zigbee Javis 12W mới
- Hỗ trợ remote điều khiển đèn gắn tường Zigbee mới

## 1.31.2-3
- Cập nhật hỗ trợ 2 model cảm biến hiện diện 24G mới

## 1.31.2-2
- Fix lỗi Daikin VRV và cảm biến hiện diện

## 1.31.2-1
- Hỗ trợ bản z2m 1.31.2
