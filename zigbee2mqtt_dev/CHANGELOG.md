
## dev
- Cập nhật phiên bản dev ngày 17/10 dựa trên z2m 1.40.2-1

## 1.36.1-5
- Thêm thuộc tính của các cảm biến hiện diện
- Đổi đúng tên 1 số thiết bị

## 1.36.1-4
- Cải tiến cảm biến hiện diện 201 và 503

## 1.36.1-3
- Hỗ trợ ổ cắm thông minh JAVIS mới
- Hỗ trợ công tắc ổ cắm JAVIS

## 1.36.1-2
- Tổ chức lại cấu trúc các thiết bị JAVIS

## 1.36.1-1
- Hỗ trợ công tắc đèn nền RGB và thay đổi độ sáng

## 1.36.1
- Cập nhật bảng z2m 1.36.1

## 1.36.0
- Cập nhật bảng z2m 1.36.0

## 1.31.2-5
- Hỗ trợ bộ đo điện 3 pha và cảm biến hiện diện 220v

## 1.31.2-4
- Hỗ trợ đèn downlight Zigbee Javis 12W mới
- Hỗ trợ remote điều khiển đèn gắn tường Zigbee mới

## 1.31.2-3
- Cập nhật hỗ trợ 2 model cảm biến hiện diện 24G mới

## 1.31.2-2
- Fix lỗi Daikin VRV và cảm biến hiện diện

## 1.31.2-1
- Hỗ trợ bản z2m 1.31.2
