## 1.0.6:
- Update

## 1.0.5:
- Bug fix

## 1.0.3:
- Fix monitor

## 1.0.2:
- Hotfix HA 2022.12.8

## 1.0.0:
- First version